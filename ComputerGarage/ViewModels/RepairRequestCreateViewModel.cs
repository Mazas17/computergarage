﻿using System.ComponentModel.DataAnnotations;

namespace ComputerGarage.ViewModels
{
    public class RepairRequestCreateViewModel
    {
        [Display(Name = "Gedimo aprašymas")]
        [Required]
        public string MalfunctionDescription { get; set; }
        [Required]
        [Display(Name = "Kompiuterio pavadinimas")]
        public string ComputerName { get; set; }
    }
}
