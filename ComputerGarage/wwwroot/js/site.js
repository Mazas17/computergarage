﻿var modal = document.getElementById('error-modal');

if (modal !== null) {
    var span = modal.getElementsByClassName('close')[0];

    span.onclick = function () {
        modal.style.display = 'none';
        span.removeAttribute('onlick');
    }
}