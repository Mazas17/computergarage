﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Security
{
    public class Roles
    {
        public const string Administrator = "Administrator";
        public const string Employee = "Employee";
        public const string Customer = "Customer";
    }
}
