﻿using ComputerGarage.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Security
{
    public class ComputerGarageUser : IdentityUser<int>
    {
        [Required]
        public Customer UserProfile { get; set; }
    }
}
