﻿using ComputerGarage.Models;
using ComputerGarage.Security;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Data
{
    public class DbInitializer
    {
        public static async Task Initialize(ComputerGarageDbContext context, UserManager<ComputerGarageUser> userManager,
            RoleManager<ComputerGarageRole> roleManager)
        {
            context.Database.EnsureCreated();
            if (context.Users.Any())
            {
                return;
            }

            await CreateDefaultUserAndRoles(userManager, roleManager);
        }

        private static async Task CreateDefaultUserAndRoles(UserManager<ComputerGarageUser> userManager,
            RoleManager<ComputerGarageRole> roleManager)
        {
            var userProfile = new Customer()
            {
                DateRegistered = DateTime.Now,
                Email = "admin@garage.lt",
                FirstName = "admin",
                LastName = "admin"
            };

            var user = new ComputerGarageUser()
            {
                Email = "admin@garage.lt",
                UserName = "admin"
            };
            user.UserProfile = userProfile;

            string password = "admin";
            await userManager.CreateAsync(user, password);

            var adminRole = new ComputerGarageRole()
            {
                Name = Roles.Administrator
            };
            var employeeRole = new ComputerGarageRole()
            {
                Name = Roles.Employee
            };
            var customerRole = new ComputerGarageRole()
            {
                Name = Roles.Customer
            };
            await roleManager.CreateAsync(adminRole);
            await roleManager.CreateAsync(employeeRole);
            await roleManager.CreateAsync(customerRole);

            await userManager.AddToRoleAsync(user, Roles.Administrator);
        }
    }
}
