﻿using ComputerGarage.Models;
using ComputerGarage.Security;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ComputerGarage.Data
{
    public class ComputerGarageDbContext : IdentityDbContext<ComputerGarageUser, ComputerGarageRole, int>
    {
        public ComputerGarageDbContext(DbContextOptions options)
            : base(options) { }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<RepairRequest> RepairRequests { get; set; }
        public virtual DbSet<RepairOrder> RepairOrders { get; set; }
        public virtual DbSet<Review> Reviews { get; set; }
        public virtual DbSet<Payment> Payments { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<EmailMessage> EmailMessages { get; set; }
    }
}
