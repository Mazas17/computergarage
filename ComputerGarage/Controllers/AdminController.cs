﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComputerGarage.Data;
using ComputerGarage.Models;
using ComputerGarage.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System.Web;

namespace ComputerGarage.Controllers
{
    public class AdminController : BaseController
    {
        public AdminController(ComputerGarageDbContext context, UserManager<ComputerGarageUser> userManager)
            : base(context, userManager) { }

        public IActionResult Open()
        {
            return View("AdminMainPage");
        }

        #region Administruoti darbuotojų paskyras
        public IActionResult OpenWorkersPage()
        {

            return View("AdminWorkersPage");
        }

        #endregion

        #region Administruoti užsakovų paskyras
        public IActionResult openCustomersPage()
        {
            return View("AdminCustomersPage");
        }
        #endregion

        #region Šalinti užsakovą

        public IActionResult openDeleteCustomer()
        {
            return View("DeleteCustomerPage");
        }

        public async Task<IActionResult> customerUserName(string username)
        {
            //findCustomer()
            var user = context.Users.Include(x => x.UserProfile).Where(x => x.Email == username).ToList();//suranda user su profile
            bool isCustomer = false;
            if(user.Count > 0)
            {
                isCustomer = await userManager.IsInRoleAsync(user[0], Roles.Customer);
            }

            if(user.Count > 0 && isCustomer)
            {
                //deleteCustomer()
                var delete = context.Customers.Find(user[0].UserProfile.Id);//istrins customers row
                context.Remove(delete);
                context.SaveChanges();

                //openCustomersPage()
                base.AddSuccess("Užsakovas sėkmingai pašalintas");
                return RedirectToAction("OpenCustomersPage");
            }

            //openDeleteCustomer()
            return RedirectToAction("openDeleteCustomer");
        }


        #endregion

        #region Redaguoti darbuotoją
        public IActionResult OpenSearch()
        {
            return View("FindUserPage");
        }

        private static int id = -1;//editinamo userio id
        public async Task<IActionResult> enterName(string username)
        {
            //find()
            var user = context.Users.Include(x => x.UserProfile).Where(x => x.Email == username).ToList();//suranda user su profile
            bool isEmployee = false;

            if (user.Count > 0)
            {
                isEmployee = await userManager.IsInRoleAsync(user[0], Roles.Employee);
            }

            if (user.Count > 0 && isEmployee)
            {
                //openUsersEditPage()
                id = user[0].Id;
                return View("AdminEditWorkerPage", user[0]);
            }

            base.AddError("Vartotojas nebuvo rastas");
            return RedirectToAction("OpenSearch");
        }

        public IActionResult editData(string name, string surename)
        {
            
            //checkData
            if(name.Length > 3 && surename.Length > 3)
            {
                //saveWorkerData()
                var user = context.Customers.Where(x => x.Id == id).ToList();//Customer table
                if(user.Count > 0)
                {
                    user[0].FirstName = name;
                    user[0].LastName = surename;

                    context.Customers.Update(user[0]);//atnaujiname customer table
                    context.SaveChanges();
                }
                //openWorkersPage()
                return View("AdminWorkersPage");
            }

            return RedirectToAction("OpenSearch");
        }
        #endregion

        #region Šalinti darbuotoją
        public IActionResult OpenDeleteWorker()
        {
            return View("DeleteUserPage");
        }

        public async Task<IActionResult> workerUserName(string username)
        {
            //find()
            var user = context.Users.Include(x => x.UserProfile).Where(x => x.Email == username).ToList();//suranda user su profile
            bool isEmployee = false;

            if(user.Count > 0)
            {
                isEmployee = await userManager.IsInRoleAsync(user[0], Roles.Employee);
            }

            if(user.Count > 0)
            {
                //deleteUser()
                var delete = context.Customers.Find(user[0].UserProfile.Id);//istrins employee row
                context.Remove(delete);
                context.SaveChanges();

                //openUsersEditPage()
                base.AddSuccess("Darbuotojas sėkmingai pašalintas");
                return RedirectToAction("OpenWorkersPage");
            }

            return View("DeleteUserPage");
        }
        #endregion

        #region Kurti darbuotoją
        public IActionResult OpenCreate()
        {
            return View("AdminCreateWorkerPage");
        }

        [HttpPost]
        public async Task<IActionResult> createWorker(string name, string surename, string email, string password, string password2)
        {
            //checkData()
            if (password != password2)
            {
                base.AddError("Įvesti slaptažodžiai nesutampa");
                return RedirectToAction("OpenCreate");
            }
            if(name == "" || surename == "" || email == "" || password == "" || password2 == "")
            {
                base.AddError("Palikote neįvestų laukų!");
                return RedirectToAction("OpenCreate");
            }

            //saveWorker()
            var newUser = new ComputerGarageUser()
            {
                Email = email,
                UserName = email,
                UserProfile = new Customer()
                {
                    FirstName = name,
                    LastName = surename,
                    Email = email
                }
            };
           // base.AddSuccess(newUser.UserProfile.FirstName);
            var result = await userManager.CreateAsync(newUser, password);
            var roleResult = await userManager.AddToRoleAsync(newUser, Roles.Employee);

            base.AddSuccess("Darbuotojas sėkmingai sukurtas");
            //openAdminPage()
            return RedirectToAction("OpenWorkersPage");
        }


        #endregion
    }
}
