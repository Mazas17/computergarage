﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComputerGarage.Data;
using ComputerGarage.Models;
using ComputerGarage.Security;
using ComputerGarage.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ComputerGarage.Controllers
{
    [Authorize(Roles = Roles.Customer + "," + Roles.Employee)]
    public class ClientController : BaseController
    {
        public ClientController(ComputerGarageDbContext context, UserManager<ComputerGarageUser> userManager)
            : base(context, userManager) { }

        #region RepairRequest
        public IActionResult OpenRepairRequestNewPage()
        {
            var model = new RepairRequestCreateViewModel();
            return View("RepairRequestNewPage", model);
        }

        [HttpPost]
        public async Task<IActionResult> EnterRequestData(RepairRequestCreateViewModel model)
        {
            if (ValidateRepairRequestData(ModelState))
            {
                var repairRequest = new RepairRequest()
                {
                    State = Models.States.RequestState.Submitted,
                    DateCreated = DateTime.Now,
                    Submitter = await userManager.FindByNameAsync(User.Identity.Name),
                    MalfunctionDescription = model.MalfunctionDescription,
                    ComputerName = model.ComputerName
                };
                context.Add(repairRequest);

                //create()
                context.SaveChanges();

                //show success message
                AddSuccess("Sekmingai issaugota");
                return RedirectToAction("OpenRepairRequestListPage");
            }
            else
            {
                //show error message
                return View("RepairRequestNewPage", model);
            }
        }
        
        public async Task<IActionResult> OpenRepairRequestListPage()
        {
            //selectAllRequests
            var repairRequests = context.Set<RepairRequest>().AsQueryable();

            var user = await userManager.FindByNameAsync(User.Identity.Name);
            if (await userManager.IsInRoleAsync(user, Roles.Customer))
            {
                repairRequests = repairRequests.Where(x => x.Submitter.Id == user.Id);
            }

            //showList()
            return View("RepairRequestListPage", repairRequests.OrderByDescending(x => x.DateCreated));
        }
        #endregion

        #region RepairOrder
        public async Task<IActionResult> OpenOrderListPage()
        {
            //selectAllOrders()
            var orders = context.Set<RepairOrder>().AsQueryable();

            var user = await userManager.FindByNameAsync(User.Identity.Name);
            if (await userManager.IsInRoleAsync(user, Roles.Customer))
            {
                orders = orders.Where(x => x.Request.Submitter.Id == user.Id);
            }

            return View("RepairOrderListPage", orders.OrderByDescending(x => x.DateCreated));
        }

        public IActionResult GetOrderPage(int id)
        {
            //find(ID=)
            var repairOrder = context.Set<RepairOrder>().Find(id);

            //showOrderPage()
            return View("RepairOrderDetailsPage", repairOrder);
        }

        [HttpPost]
        public IActionResult ConfirmOrder(int id)
        {
            var repairOrder = context.Set<RepairOrder>().Find(id);

            //update()
            repairOrder.State = Models.States.RepairOrderState.Confirmed;
            context.SaveChanges();

            //order confirmed
            base.AddSuccess("Užsakymas sėkmingai patvirtinas");

            return RedirectToAction("OpenOrderListPage");
        }
        #endregion

        private bool ValidateRepairRequestData(ModelStateDictionary modelState)
        {
            return modelState.IsValid;
        }
    }
}