﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ComputerGarage.Models;
using Microsoft.AspNetCore.Identity;
using ComputerGarage.Security;
using Microsoft.AspNetCore.Authorization;
using ComputerGarage.Data;

namespace ComputerGarage.Controllers
{
    public class HomeController : BaseController
    {
        private readonly SignInManager<ComputerGarageUser> signInManager;
        private readonly RoleManager<ComputerGarageRole> roleManager;

        public HomeController(
            ComputerGarageDbContext context, 
            UserManager<ComputerGarageUser> userManager,
            SignInManager<ComputerGarageUser> signInManager,
            RoleManager<ComputerGarageRole> roleManager) : base(context, userManager)
        {
            this.signInManager = signInManager;
            this.roleManager = roleManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await userManager.GetUserAsync(User);
            ViewData["msg"] = "hello " + user?.Email;

            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Login(string username, string password)
        {
            var user = await userManager.FindByNameAsync(username);

            if (user != null )
            {
                var result = await signInManager.PasswordSignInAsync(user, password, false, false);

                if(!result.Succeeded)
                {
                    ModelState.AddModelError(string.Empty, "Prisijungti nepavyko");
                    return View();
                }
            }
            else
            {
                base.AddError("Toks vartotojas nerastas");
                return View();
            }

            base.AddSuccess("Sėkmingai prisijungta");
            return RedirectToAction("Index");
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
