﻿using System;
using System.Linq;
using ComputerGarage.Data;
using ComputerGarage.Security;
using ComputerGarage.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;

namespace ComputerGarage.Controllers
{
    [Authorize(Roles = Roles.Customer)]
    public class EditProfileController : BaseController
    {
        public EditProfileController(ComputerGarageDbContext context, UserManager<ComputerGarageUser> userManager) 
            : base(context, userManager)
        {
        }

        public IActionResult OpenEditProfilePage()
        {
            var user = context.Users
                .Include(x => x.UserProfile)
                .Single(x => x.UserName == User.Identity.Name);

            return View("EditProfilePage", new CustomerProfileViewModel()
            {
                FirstName = user.UserProfile.FirstName,
                LastName = user.UserProfile.LastName
            });
        }

        [HttpPost]
        public IActionResult Edit(CustomerProfileViewModel model)
        {
            //validate()
            if (Validate(this.ModelState))
            {
                //editUser()
                var user = context.Users
                    .Include(x => x.UserProfile)
                    .Single(x => x.UserName == User.Identity.Name);

                user.UserProfile.FirstName = model.FirstName;
                user.UserProfile.LastName = model.LastName;

                context.SaveChanges();
            }
            else
            {
                return View("EditProfilePage", model);
            }

            base.AddSuccess("Sėkmingai išsaugota");
            return RedirectToAction("OpenEditProfilePage", "EditProfile");
        }

        private bool Validate(ModelStateDictionary modelState) => modelState.IsValid;
    }
}
