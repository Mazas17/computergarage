﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using ComputerGarage.Data;
using ComputerGarage.Models;
using ComputerGarage.Security;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ComputerGarage.Controllers
{
    public class RegisterController : BaseController
    {
        private readonly SignInManager<ComputerGarageUser> signInManager;
        private readonly RoleManager<ComputerGarageRole> roleManager;

        public RegisterController(
            ComputerGarageDbContext context,
            UserManager<ComputerGarageUser> userManager,
            SignInManager<ComputerGarageUser> signInManager,
            RoleManager<ComputerGarageRole> roleManager) : base(context, userManager)
        {
            this.signInManager = signInManager;
            this.roleManager = roleManager;
        }

        public IActionResult OpenRegisterPage()
        {
            return View("RegisterPage");
        }

        [HttpPost]
        public async Task<IActionResult> Register(string firstName, string lastName, string email, string password, string password2)
        {
            if (password != password2)
            {
                ModelState.AddModelError(string.Empty, "Passwords doesnt match");
                return View();
            }

            var newUser = new ComputerGarageUser()
            {
                Email = email,
                UserName = email
            };

            var userProfile = new Customer()
            {
                DateRegistered = DateTime.Now,
                Email = email,
                FirstName = firstName,
                LastName = lastName

            };

            if (lastName == firstName)
            {
                throw new IllegalArgumentException("Patikrinti vartotojo vardą ir pavardę.");
            }

            newUser.UserProfile = userProfile;

            //registerUser()
            var isUnique = await userManager.FindByEmailAsync(email);
            if (isUnique != null)
            {
                throw new IllegalArgumentException("Jau yra vartotojas su tokiu el. pastu, seniuk.");
            }
            else
            {
                var result = await userManager.CreateAsync(newUser, password);
                var roleResult = await userManager.AddToRoleAsync(newUser, Roles.Customer);

                var isRegistered = await userManager.FindByEmailAsync(email);
                if (isRegistered == null)
                {
                    throw new IllegalArgumentException("Isivele klaida ir vartotojas nebuvo sukurtas.");
                }

                //validate()
                if (Validate(result, roleResult))//duomenys teisingi
                {
                    var user = await userManager.FindByEmailAsync(email);
                    //registerUser()
                    await signInManager.SignInAsync(user, true);
                    //openMainPage()
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return View();
                }
            }
        }

        private bool Validate(IdentityResult userResult, IdentityResult roleResult)
        {
            bool success = true;
            if (!userResult.Succeeded)
            {
                success = false;
                foreach (var error in userResult.Errors)
                    ModelState.AddModelError(string.Empty, error.Description);
            }

            if (!roleResult.Succeeded)
            {
                success = false;
                foreach (var error in roleResult.Errors)
                    ModelState.AddModelError(string.Empty, error.Description);
            }
            return success;
        }
    }

    [Serializable]
    internal class IllegalArgumentException : Exception
    {
        public IllegalArgumentException()
        {
        }

        public IllegalArgumentException(string message) : base(message)
        {
        }

        public IllegalArgumentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IllegalArgumentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}