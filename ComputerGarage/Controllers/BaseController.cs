﻿using ComputerGarage.Data;
using ComputerGarage.Security;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ComputerGarage.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly ComputerGarageDbContext context;
        protected readonly UserManager<ComputerGarageUser> userManager;

        public BaseController(ComputerGarageDbContext context, UserManager<ComputerGarageUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        protected void AddError(string message)
        {
            TempData["Error"] = message;
        }

        protected void AddSuccess(string message)
        {
            TempData["Success"] = message;
        }
    }
}