﻿using ComputerGarage.Data;
using ComputerGarage.Models;
using ComputerGarage.Models.States;
using ComputerGarage.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Controllers
{
    [Authorize(Roles = Roles.Employee)]
    public class RepairController : BaseController
    {
        public RepairController(ComputerGarageDbContext context,
            UserManager<ComputerGarageUser> userManager) : base(context, userManager)
        { }
        
        public IActionResult GetEquipmentPage()
        {
            var repairOrders = context.Set<RepairOrder>()
                .Where(x => x.State == RepairOrderState.Confirmed)
                .Select(x => new SelectListItem
                {
                    Text = x.Request.ComputerName + " " + x.DateCreated.ToShortDateString(),
                    Value = x.Id.ToString()
                }).ToList();

            return View("RegisterEquipmentPage", repairOrders);
        }
        
        [HttpPost]
        public IActionResult RegisterReceivedEquipment(int id)
        {
            var repairOrder = context.Set<RepairOrder>().Find(id);
            //changeOrderState()
            repairOrder.State = RepairOrderState.DeliveredToGarage;
            context.SaveChanges();

            //success message
            base.AddSuccess("Įranga užregistruota");
            return RedirectToAction("GetEquipmentPage");
        }

        public IActionResult OpenRepairPage()
        {
            //open()
            return View("AdminRepairPage", context.Set<RepairRequest>());
        }
    }
}
