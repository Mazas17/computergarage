﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComputerGarage.Data;
using ComputerGarage.Models;
using ComputerGarage.Security;
using ComputerGarage.Services;
using ComputerGarage.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ComputerGarage.Controllers
{
    [Authorize(Roles = Roles.Employee)]
    public class EmailController : BaseController
    {
        private readonly IEmailService emailService;

        public EmailController(ComputerGarageDbContext context, UserManager<ComputerGarageUser> userManager, IEmailService emailService)
            : base(context, userManager)
        {
            this.emailService = emailService;
        }

        public IActionResult OpenEmailPage() => View("SendEmailMessagePage");

        [HttpPost]
        public IActionResult Send(string emailTo, string text)
        {
            //send()
            bool result = emailService.SendEmail(emailTo, text);
            if (result)
            {
                base.AddSuccess("Laiskas sekmingai issiustas");
            }
            else
            {
                base.AddSuccess("Laisko nepaviko issiusti");
            }
            return RedirectToAction("OpenEmailPage");
        }
    }
}
