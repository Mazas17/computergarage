﻿using ComputerGarage.Models.States;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models
{
    public class EmailMessage : BaseEntity
    {
        public string ReceiverEmail { get; set; }
        public string Header { get; set; }
        public string Text { get; set; }
        public MessageState State { get; set; }
        public DateTime SentDate { get; set; }
        
        public RepairOrder RepairOrder { get; set; }
        public RepairRequest RepairRequest { get; set; }
    }
}
