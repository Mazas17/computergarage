﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models.States
{
    public enum RequestState
    {
        /// <summary>
        /// atmestas
        /// </summary>
        [Display(Name = "Atmesta")]
        Rejected = 1,
        /// <summary>
        /// pateiktas
        /// </summary>
        [Display(Name = "Pateikta")]
        Submitted = 2,
        /// <summary>
        /// patvirtintas
        /// </summary>
        [Display(Name = "Patvirtinta")]
        Confirmed = 3
    }
}
