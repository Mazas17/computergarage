﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models.States
{
    public enum MessageState
    {
        Sent,
        NotSent,
        Created
    }
}
