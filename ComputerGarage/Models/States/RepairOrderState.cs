﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models.States
{
    public enum RepairOrderState
    {
        /// <summary>
        /// apmoketas
        /// </summary>
        [Display(Name = "Apmokėtas")]
        Paid,
        /// <summary>
        /// patvirtintas
        /// </summary>
        [Display(Name = "Patvirtintas")]
        Confirmed,
        /// <summary>
        /// pristatytas i servisa
        /// </summary>
        [Display(Name = "Pristatytas į servisą")]
        DeliveredToGarage,
        /// <summary>
        /// sukurtas
        /// </summary>
        [Display(Name = "Sukurtas")]
        Created,
        /// <summary>
        /// vykdomas
        /// </summary>
        [Display(Name = "Vykdomas")]
        InProgress,
        /// <summary>
        /// ivykdytas
        /// </summary>
        [Display(Name = "Įvykdytas")]
        Done
    }
}
