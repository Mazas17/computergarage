﻿using ComputerGarage.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models
{
    public class Review : BaseEntity
    {
        public DateTime Date { get; set; }
        public string Text { get; set; }
        public string Author { get; set; }

        /// <summary>
        /// kas parase Review
        /// </summary>
        public ComputerGarageUser Submitter { get; set; }
    }
}
