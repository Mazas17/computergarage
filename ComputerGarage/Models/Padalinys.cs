﻿using ComputerGarage.Models.States;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models
{
    public class Padalinys : BaseEntity
    {
        public string pavadinimas { get; set; }
        public string adresas { get; set; }

        public Padalinys(string pavadinimas, string adresas)
        {
            this.pavadinimas = pavadinimas;
            this.adresas = adresas;
        }

        private Employee employee;

        public Padalinys(Employee employee)
        {
            this.employee = employee;
        }
    }
}
