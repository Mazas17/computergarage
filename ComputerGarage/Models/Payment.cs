﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models
{
    public class Payment : BaseEntity
    {
        public string PayerCode { get; set; }
        public float PayedSum { get; set; }
        public DateTime PaymentDate { get; set; }

        [Required]
        public RepairOrder RepairOrder { get; set; }
    }
}
