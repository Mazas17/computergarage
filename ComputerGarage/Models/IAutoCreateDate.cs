﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models
{
    interface IAutoCreateDate
    {
        DateTime DateCreated { get; set; }
    }
}
