﻿using ComputerGarage.Models.States;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models
{
    public class RepairOrder : BaseEntity
    {
        [Display(Name = "Būsena")]
        [Required]
        public RepairOrderState State { get; set; }
        [Display(Name = "Sukūrimo data")]
        [Required]
        public DateTime DateCreated { get; set; }
        [Display(Name = "Remonto kaina")]
        [Required]
        public float RepairPrice { get; set; }

        [Required]
        public RepairRequest Request { get; set; }

        public virtual IList<Payment> Payments { get; set; }
        public virtual IList<EmailMessage> EmailMessages { get; set; }

    }
}
