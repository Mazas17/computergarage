﻿using ComputerGarage.Models.States;
using ComputerGarage.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Models
{
    public class RepairRequest : BaseEntity, IAutoCreateDate
    {
        [Display(Name = "Būsena")]
        public RequestState State { get; set; }
        [Display(Name = "Sukūrimo data")]
        public DateTime DateCreated { get; set; }
        /// <summary>
        /// gedimo aprassymas
        /// </summary>
        [Display(Name = "Gedimo aprašymas")]
        [Required]
        public string MalfunctionDescription { get; set; }
        [Required]
        [Display(Name = "Kompiuterio pavadinimas")]
        public string ComputerName { get; set; }

        /// <summary>
        /// Vartotojas,kuris pateike remonto uzklausa
        /// </summary>
        [Required]
        public ComputerGarageUser Submitter { get; set; }

        public virtual IList<RepairOrder> RepairOrders { get; set; }
    }
}
