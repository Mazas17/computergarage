﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerGarage.Extensions
{
    public static class HtmlExtensions
    {
        public static IHtmlContent SuccessMessage(this IHtmlHelper helper)
        {
            string message = (string)helper.TempData["Success"];
            return ModalWindow(helper, message, "success");
        }

        public static IHtmlContent ErrorMessage(this IHtmlHelper helper)
        {
            string message = (string)helper.TempData["Error"];
            return ModalWindow(helper, message, "error");
        }

        private static IHtmlContent ModalWindow(IHtmlHelper helper, string message, string htmlContentClass)
        {
            if (!string.IsNullOrEmpty(message))
            {
                StringBuilder builder = new StringBuilder();
                builder.Append("<div class=\"modal-message\" id=\"error-modal\">");
                builder.AppendFormat("<div class=\"modal-message-content {0}\">", htmlContentClass);
                builder.Append("<span class=\"close\">&times;</span>");
                builder.AppendFormat("<h3>{0}</h3>", message);
                builder.Append("</div>");
                builder.Append("</div>");

                return new HtmlString(builder.ToString());
            }
            return new HtmlString(string.Empty);
        }
    }
}
