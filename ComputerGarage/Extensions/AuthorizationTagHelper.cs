﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Linq;

namespace ComputerGarage.Extensions
{
    [HtmlTargetElement(Attributes = "asp-authorize")]
    [HtmlTargetElement(Attributes = "asp-authorize,asp-roles")]
    public class AuthorizationTagHelper : TagHelper
    {
        [HtmlAttributeName("asp-roles")]
        public string Roles { get; set; } = string.Empty;

        [HtmlAttributeName("asp-authorize")]
        public bool Authorize { get; set; }

        private readonly IHttpContextAccessor httpContextAccessor;

        public AuthorizationTagHelper(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (!Authorize && Roles != string.Empty)
                throw new Exception("Must be authorized to use role");

            var user = httpContextAccessor.HttpContext.User;
            bool anyRoleMatch = Roles == string.Empty || Roles.Split(",").Any(role => user.IsInRole(role));
            if (Authorize != user.Identity.IsAuthenticated || !anyRoleMatch)
            {
                output.SuppressOutput();
            }
        }
    }
}
