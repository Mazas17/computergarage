﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Services
{
    public interface IEmailService
    {
        bool SendEmail(string toEmail, string message);
    }
}
