﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ComputerGarage.Services
{
    public class EmailService : IEmailService
    {
        public bool SendEmail(string toEmail, string message)
        {
            Console.WriteLine($"Sending email message to {toEmail}: {message}");
            return true;
        }
    }
}
